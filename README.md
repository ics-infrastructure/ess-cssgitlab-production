# ESS-CSSGitlab-Production

Repository to test CS-Studio production pipeline in GitLab

Following CI/CD variables are set:

- REPO_BRANCH: production
- DISPLAY_BUILDER_REPO_BRANCH: production
- ARTIFACTORY_FOLDER: CSSGitlab/production
